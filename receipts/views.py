from django.shortcuts import render, redirect
from .models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from .form import ReceiptForm, ExpensesCategoryForm, AccountForm


# Create your views here.
@login_required
def receipts(request):
    receipts_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt": receipts_list}
    return render(request, "receipt/receipt_list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipt/create.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts": accounts}
    return render(request, "receipt/accounts.html", context)


@login_required
def category_list(request):
    expensescategory = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expensescategory": expensescategory}
    return render(request, "receipt/expensescategory.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpensesCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(commit=False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category")
    else:
        form = ExpensesCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipt/createcategory.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            accounts = form.save(False)
            accounts.owner = request.user
            accounts.save()
            return redirect("accounts")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipt/createaccount.html", context)
