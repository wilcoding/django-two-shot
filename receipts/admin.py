from django.contrib import admin
from .models import Receipt, Account, ExpenseCategory


# Register your models here.
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )


admin.site.register(Receipt, ReceiptAdmin)


class AccountAdmin(admin.ModelAdmin):
    list_display = ("name", "number", "owner")


admin.site.register(Account, AccountAdmin)


class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "owner")


admin.site.register(ExpenseCategory, ExpenseCategoryAdmin)
